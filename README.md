calc
====

A web-browser-based calculator for physicists.  
 - physical constants
 - units
 - works off-line

 
####Instructions
Simply open calc.html in the web browser of your choice.

####Tested in
 - Firefox 20+ for mac.
 - Safari 5+

